FROM php:8.2-apache

RUN apt-get update && apt-get install -my --no-install-recommends \
    wget \
    gnupg \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libwebp-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    zlib1g \
    ssh \
    sshpass \
    libc-client-dev \
    libkrb5-dev \
    libzip-dev \
    unzip \
    libicu-dev \
    libmagickwand-dev \
    exim4 \
    git \
    nodejs \
    npm \
    rsync\
    jq\
    mariadb-client

RUN docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp
RUN docker-php-ext-configure opcache --enable-opcache
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl
RUN docker-php-ext-configure intl
RUN docker-php-ext-install -j$(nproc) iconv gd opcache soap zip imap mysqli pdo pdo_mysql exif intl
RUN pecl install imagick
RUN pecl install xdebug
RUN pecl install redis
RUN docker-php-ext-enable imagick xdebug redis.so

RUN rm -rf /var/lib/apt/lists/* \
    && a2enmod rewrite headers expires \
    && mkdir -p /var/www/public_html \
    && ln -s /var/www/public_html /var/www/www \
    && mkdir -p /var/www/www

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#used for encore
RUN npm cache clean --force \
    && npm install -g n \
    && n stable

RUN npm install -g snyk

RUN curl -s -L https://visma.polaris.synopsys.com/api/tools/v2/downloads/polaris_cli-linux64-2024.3.0.zip  >  polaris.zip \
    && unzip polaris.zip \
    && cp ./po*/bin/polaris /bin

# Copy configuration
COPY config/php/opcache.ini $PHP_INI_DIR/conf.d/
COPY config/php/php.ini /usr/local/etc/php/php.ini
COPY config/xdebug/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
COPY config/apache/apache.conf /etc/apache2/sites-enabled/000-default.conf
COPY config/apache/ports.conf /etc/apache2/ports.conf
COPY config/apache/ports.conf /etc/apache2/ports.conf
COPY config/exim4/exim4.conf /etc/exim4/exim4.conf
